public class Main {

    public static void main(String[] args) {
        LinkedList linkedlist = new LinkedList();
        linkedlist.append(5);
        linkedlist.printAllElementsInLinkedList();
        linkedlist.append(2);
        linkedlist.printAllElementsInLinkedList();
        linkedlist.append(4);
        linkedlist.printAllElementsInLinkedList();
        linkedlist.deleteAnElementWithValue(4);
        linkedlist.printAllElementsInLinkedList();
        linkedlist.findIndexOfAnElementByValue(5);
        linkedlist.findIndexOfAnElementByValue(11);
        linkedlist.prepend(123);
        linkedlist.findIndexOfAnElementByValue(123);
        linkedlist.findIndexOfAnElementByValue(5);
    }
}
