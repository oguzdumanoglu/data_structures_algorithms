public class LinkedList {
    Node head;

    public void append (int data) {
        if (head == null) {
            head = new Node(data);
            System.out.println(data + " is prepended to linked list");
            return;
        }
        Node current = head;
        while (current.next != null) {
            current = current.next;
        }
        if (current.next == null) {
            Node newNode = new Node(data);
            System.out.println(data + " is appended to linked list");
            current.next = newNode;
            return;
        }
    }

    public void prepend (int data) {
        if (head == null) {
            head = new Node(data);
        } else {
            Node newNode = new Node(data);
            System.out.println(data + " is prepended to linked list");
            newNode.next = head;
            head = newNode;
        }
    }

    public void deleteAnElementWithValue (int value) {
        if (head == null) {
            System.out.println("There are no elements in the list.");
            return;
        }
        if (head.data == value) {
            head.next = head;
            System.out.println("Our Head Node was deleted.");
            System.out.println("Our new Head Node's value is " + value);
        }
        Node current = head;
        while (current.next != null) {
            current = current.next;
            if (current.next.data == value) {
                System.out.println(value + " is deleted.");
                current.next = current.next.next;
            }
        }
    }

    public void printAllElementsInLinkedList () {
        if (head == null) {
            System.out.println("There is nothing to display in the Linked List");
        }
        Node current = head;

        while (current != null) {
            System.out.println(current.data);
            current = current.next;
        }
    }

    public int findIndexOfAnElementByValue (int value) {
        if (head == null) {
            System.out.println("Not Found, there are no elements in the list.");
            return -1;
        }

        int index_counter = 0;

        if (head.data == value) {
            System.out.println(value +" is found at " + index_counter + " index");
            return 0;
        }

        Node current = head;

        while (current.next != null) {
            current = current.next;
            index_counter++;

            if (current.data == value) {
                System.out.println("It is found at " + index_counter + " index");
                return index_counter;
            }
        }
        System.out.println(value + " is not found, it is not on the list.");
        return -1;
    }

}
